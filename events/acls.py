import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY



def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state}
    url = "https://api.pexels.com/v1/search?query=nature&per_page=1"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return{"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError):
        return {"picture_url": None}



def get_weather_data(city, state):
    headers = {"Authorization", OPEN_WEATHER_API_KEY}

    geocode_url = "http://api.openweathermap.org/geo/1.0/direct?q={city name},{state code},{country code}&limit={limit}&appid={API key}"
    params = {
        "q": city + " " + state,
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    response = requests.get(geocode_url, params=params, headers=headers)
    content = json.loads(response.content)


    weather_url = "https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}"
    lat = content[0]["lat"]
    lon = content[0]["lon"]
    params = {
        "lat": "latitude",
        "lon": "longitude",
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(weather_url, params=params, headers=headers)
    weather_content = json.loads(response.content)

    weather = {
        "temperature": weather_content["main"]["temp"],
        "description": weather_content["weather"][0]["description"]
    }

    return weather
